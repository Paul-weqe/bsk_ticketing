package com.example.bsk_ticketing

import com.example.bsk_ticketing.config.properties.JasyptProperties
import com.example.bsk_ticketing.service.TicketService
import lombok.extern.java.Log
import org.jasypt.encryption.StringEncryptor
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.test.context.junit4.SpringRunner
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.logging.Level
import java.util.logging.Logger

@Log
@RunWith(SpringRunner::class)
@SpringBootTest
class BskTicketingApplicationTests {

    @Autowired
    lateinit var jasyptEncryptor: StringEncryptor

    @Autowired
    lateinit var ticketService: TicketService

    @Test
    fun testBskTicketing() {
        Logger.getLogger(javaClass.name).log(Level.INFO, "BCryptPasswordEncoder: " + BCryptPasswordEncoder().encode("AdminDoe1234"))
        Logger.getLogger(javaClass.name).log(Level.INFO, "JasyptStringEncryptor: " + jasyptEncryptor.encrypt("Test1234"))
        Logger.getLogger(javaClass.name).log(Level.INFO, "JasyptStringEncryptor: " + jasyptEncryptor.decrypt("Cmb8efHGXl4XO99A0MtEBwDdpxGBHGXB"))
    }

    @Test
    fun testTicketService() {
        ticketService.downloadTicketReportsWithoutStatus(
                SimpleDateFormat("dd/MM/yyyy").parse("01/01/2019"),
                SimpleDateFormat("dd/MM/yyyy").parse("31/12/2019"))
                .forEach { ticket -> Logger.getLogger(javaClass.name).log(Level.INFO, ticket.toString()) }
    }

    @Test
    fun testGetTicketsForDownload() {
        Logger.getLogger(javaClass.name).log(Level.INFO, Timestamp.valueOf(SimpleDateFormat("dd/MMM/yyyy").format(Timestamp.from(Instant.now()))).toString())
    }

}
