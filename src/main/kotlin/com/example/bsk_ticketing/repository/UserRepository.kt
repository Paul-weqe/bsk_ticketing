package com.example.bsk_ticketing.repository

import com.example.bsk_ticketing.model.User
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Component
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : CrudRepository<User, Int> {
    fun findByEmail(email: String): User?
    fun findByUsername(name: String): User?
    fun findByEmailAndPassword(email: String, password: String): User?
    fun findAllByRole(role: String): ArrayList<User>
    fun findAllByRoleOrRole(role: String, role1: String): ArrayList<User>
}