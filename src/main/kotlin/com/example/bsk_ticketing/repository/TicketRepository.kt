package com.example.bsk_ticketing.repository

import com.example.bsk_ticketing.model.Ticket
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface TicketRepository : CrudRepository<Ticket, Int>{
    fun findAll(pageable: Pageable): Page<Ticket>
    fun findAllByUserId(pageable: Pageable, id: Int): Page<Ticket>
    fun findAllByAssetId(assetId: Int): ArrayList<Ticket>
    fun findAllByIsOpen(pageable: Pageable, isOpen: Boolean): Page<Ticket>
    fun findAllByUserIdAndIsOpen(pageable: Pageable, id: Int, isOpen: Boolean): Page<Ticket>
    fun findAllByIsOpenAndOpenedAtGreaterThanEqualAndOpenedAtLessThanEqual(
            isOpen: Boolean, startDate: Date, endDate: Date): ArrayList<Ticket>
    fun findAllByOpenedAtGreaterThanEqualAndOpenedAtLessThanEqual(
            startDate: Date, endDate: Date): ArrayList<Ticket>
}
