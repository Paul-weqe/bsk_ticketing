package com.example.bsk_ticketing.repository

import com.example.bsk_ticketing.model.Asset
import org.springframework.data.repository.CrudRepository

interface AssetRepository : CrudRepository<Asset, Int>