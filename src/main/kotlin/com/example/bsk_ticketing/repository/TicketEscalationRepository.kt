package com.example.bsk_ticketing.repository

import com.example.bsk_ticketing.model.TicketEscDetails
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface TicketEscalationRepository: CrudRepository<TicketEscDetails, Int> {
    fun findAllByEscalateeId(escalateeId: Int): ArrayList<TicketEscDetails>
}