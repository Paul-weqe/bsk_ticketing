package com.example.bsk_ticketing.model

import lombok.Data
import javax.persistence.*

@Data
@Entity
@Table(name="ticket_escalation_details")
class TicketEscDetails {
    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Int = 0

    @ManyToOne
    @JoinColumn(name = "ticket_id", referencedColumnName = "id")
    var ticket: Ticket = Ticket()

    @Basic
    @Column(name = "escalator_id")
    var escalatorId = 0

    @Basic
    @Column(name = "escalatee_id")
    var escalateeId = 0

}