package com.example.bsk_ticketing.model

import lombok.Data
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import java.io.Serializable
import java.util.logging.Level
import java.util.logging.Logger
import javax.persistence.*
import javax.persistence.FetchType



@Data
@Entity
@Table(name="users")
class User: Serializable{

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Int = 0

    @Basic
    @Column(name="first_name")
    var firstName: String = ""

    @Basic
    @Column(name="last_name")
    var lastName: String = ""

    @Basic
    @Column(name="email", unique = true)
    var email: String = ""

    @Basic
    @Column(name="password")
    var password: String = ""

    @Basic
    @Column(name="username", unique = true)
    var username: String = ""

    @Basic
    @Column(name="role")
    var role: String = "PLACEHOLDER"

    @Basic
    @Column(name="pass_approved")
    var passApproved: String? = "PLACEHOLDER"

    @OneToMany(mappedBy = "userid", fetch = FetchType.EAGER)
    private val usersRoles: Collection<UserRoles>? = null

    @Transient
    fun getGrantedAuthorities(): List<GrantedAuthority> {
        usersRoles!!.forEach {
            role -> Logger.getLogger(javaClass.name).log(Level.INFO, "Roles: ${role.roleid!!.authority}")
        }
        val authorities: MutableList<GrantedAuthority>? = mutableListOf()
        usersRoles.forEach {
            role -> authorities!!.add(SimpleGrantedAuthority(role.roleid!!.authority))
        }
        return authorities as List<GrantedAuthority>
    }
}