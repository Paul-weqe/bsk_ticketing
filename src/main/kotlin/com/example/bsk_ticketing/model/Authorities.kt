package com.example.bsk_ticketing.model

import lombok.Data
import lombok.NoArgsConstructor
import lombok.ToString
import java.io.Serializable
import java.sql.Timestamp
import javax.persistence.*


@Data
@Entity
@Table(name = "adm_authorities")
@NoArgsConstructor
class Authorities: Serializable {
    @Id
    @SequenceGenerator(name = "adm_authorities_id_seq_gen", sequenceName = "adm_authorities_id_seq", allocationSize = 1)
    @GeneratedValue(generator = "adm_authorities_id_seq_gen", strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    val id: Int? = null
    @Basic
    @Column(name = "authority")
    val authority: String? = null
    @Basic
    @Column(name = "status")
    val status: Int? = null
    @Basic
    @Column(name = "var_field_1")
    val varField1: String? = null
    @Basic
    @Column(name = "var_field_2")
    val varField2: String? = null
    @Basic
    @Column(name = "var_field_3")
    val varField3: String? = null
    @Basic
    @Column(name = "var_field_4")
    val varField4: String? = null
    @Basic
    @Column(name = "var_field_5")
    val varField5: String? = null
    @Basic
    @Column(name = "var_field_6")
    val varField6: String? = null
    @Basic
    @Column(name = "var_field_7")
    val varField7: String? = null
    @Basic
    @Column(name = "var_field_8")
    val varField8: String? = null
    @Basic
    @Column(name = "var_field_9")
    val varField9: String? = null
    @Basic
    @Column(name = "var_field_10")
    val varField10: String? = null
    @Basic
    @Column(name = "created_by")
    val createdBy: String? = null
    @Basic
    @Column(name = "created_on")
    val createdOn: Timestamp? = null
    @Basic
    @Column(name = "modified_by")
    val modifiedBy: String? = null
    @Basic
    @Column(name = "modified_on")
    val modifiedOn: Timestamp? = null
    @Basic
    @Column(name = "delete_by")
    val deleteBy: String? = null
    @Basic
    @Column(name = "deleted_on")
    val deletedOn: Timestamp? = null
    @Basic
    @Column(name = "application_id")
    val applicationId: Int? = null
}