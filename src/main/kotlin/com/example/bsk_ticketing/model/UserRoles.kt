package com.example.bsk_ticketing.model

import lombok.Data
import lombok.NoArgsConstructor
import lombok.ToString
import java.io.Serializable
import java.security.Timestamp
import javax.persistence.*


@Data
@Entity
@Table(name = "adm_user_roles")
@NoArgsConstructor
class UserRoles: Serializable {
    @Id
    @SequenceGenerator(name = "adm_user_roles_id_seq_gen", sequenceName = "adm_user_roles_id_seq", allocationSize = 1)
    @GeneratedValue(generator = "adm_user_roles_id_seq_gen", strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private val id: Int? = null
    @Basic
    @Column(name = "status")
    private val status: Int? = null
    @Basic
    @Column(name = "createdby")
    private val createdBy: String? = null
    @Basic
    @Column(name = "createddate")
    private val createdOn: Timestamp? = null
    @Basic
    @Column(name = "modifiedby")
    private val modifiedBy: String? = null
    @Basic
    @Column(name = "modifieddate")
    private val modifiedOn: Timestamp? = null
    @Basic
    @Column(name = "userid")
    private val userid: Int? = null
    @ManyToOne
    @JoinColumn(name = "roleid", referencedColumnName = "ID")
    var roleid: Authorities? = null

}