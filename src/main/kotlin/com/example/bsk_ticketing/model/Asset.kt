package com.example.bsk_ticketing.model

import lombok.Data
import javax.persistence.*

@Data
@Entity
@Table(name="assets")
class Asset{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Int = 0

    @Column(name = "")
    @Basic
    var name: String = ""

    @Column(name = "", nullable = true)
    @Basic
    var description: String = ""

}
