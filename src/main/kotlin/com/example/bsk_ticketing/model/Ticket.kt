package com.example.bsk_ticketing.model

import lombok.Data
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*
import javax.persistence.*

@Data
@Entity
@Table(name="tickets")
class Ticket {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Int = 0

    @Column(name = "title")
    @Basic
    var title: String = ""

    @Column(name="description")
    @Basic
    var description: String = ""

    @Column(name = "user_id")
    @Basic
    var userId: Int = 0

    @Column(name = "user_email")
    @Basic
    var userEmail: String = ""

    @Column(name = "user_first_name")
    var userFirstName: String = ""

    @Column(name = "user_last_name")
    var userLastName: String = ""

    @Column(name = "is_open")
    @Basic
    var isOpen: Boolean = true

    @Column(name = "opened_at")
    var openedAt: Date = Date()

    @Column(name = "created_on")
    var createdOn: String? = SimpleDateFormat("dd-MMM-yyyy").format(openedAt)

    @Column(name = "closed_by_id")
    var closedById: Int = 0

    @Column(name = "closed_by_email")
    var closedByEmail: String = ""

    @Column(name = "closed_by_first_name")
    var closedByFirstName: String = ""

    @Column(name = "closed_by_last_name")
    var closedByLastName: String = ""

    @Column(name = "closed_at", nullable = true)
    var closedAt: Date? = null

    @Column(name = "resolved_at", nullable = true)
    var resolvedAt: String? = if (closedAt != null) SimpleDateFormat("dd-MMM-yyyy").format(closedAt) else ""

    @Column(name = "asset_name", nullable = true)
    var assetName: String = ""

    @Column(name = "asset_id", nullable = true)
    var assetId: Int = 0

    @Column(name = "opened_status")
    var openedStatus: Int = 0

    @Column(name = "engaged")
    var engaged: Int = 0

}