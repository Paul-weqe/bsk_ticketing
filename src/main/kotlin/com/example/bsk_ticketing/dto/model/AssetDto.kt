package com.example.bsk_ticketing.dto.model

data class AssetDto (
        var id: Int = 0,
        var name: String = "",
        var description: String = ""
)