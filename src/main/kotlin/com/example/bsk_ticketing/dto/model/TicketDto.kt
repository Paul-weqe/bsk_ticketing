package com.example.bsk_ticketing.dto.model

import java.sql.Timestamp
import java.util.*

data class TicketDto(
        var id: Int = 0,
        var title: String = "",
        var description: String = "",
        var userId: Int = 0,
        var userEmail: String = "",
        var userFirstName: String = "",
        var userLastName: String = "",
        var isOpen: Boolean = true,
        var openedAt: Date = Date(),
        var openedAtLessTime: String? = "",
        var closedById: Int = 0,
        var closedByEmail: String = "",
        var closedByFirstName: String = "",
        var closedByLastName: String = "",
        var closedAt: Date?= null,
        var closedAtLessTime: String? = "",
        var assetName: String = "",
        var assetId: Int = 0,
        var openedStatus: Int = 1,
        var engaged: Int = 0
)