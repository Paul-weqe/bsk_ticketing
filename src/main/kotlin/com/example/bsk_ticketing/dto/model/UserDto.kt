package com.example.bsk_ticketing.dto.model

data class UserDto(
        var id: Int = 0,
        var firstName: String = "",
        var lastName: String = "",
        var email: String = "",
        var password: String = "",
        var username: String = "",
        var passApproved: String? = "PLACEHOLDER",
        var role: String = "PLACEHOLDER"
)