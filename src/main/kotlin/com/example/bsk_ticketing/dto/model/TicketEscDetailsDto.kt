package com.example.bsk_ticketing.dto.model

import com.example.bsk_ticketing.model.Ticket

class TicketEscDetailsDto {

    var id: Int = 0
    var ticket: Ticket = Ticket()
    var escalatorId = 0
    var escalateeId = 0
}