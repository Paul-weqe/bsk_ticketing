package com.example.bsk_ticketing.dto.mapper

import com.example.bsk_ticketing.dto.model.TicketEscDetailsDto
import com.example.bsk_ticketing.model.TicketEscDetails

class TicketEscDetailsMapper {

    companion object {
        fun toTicketEscDetailsDto(ticketEscDetails: TicketEscDetails): TicketEscDetailsDto {
            return TicketEscDetailsDto().apply {
                    id = ticketEscDetails.id
                    ticket = ticketEscDetails.ticket
                    escalatorId = ticketEscDetails.escalatorId
                    escalateeId = ticketEscDetails.escalateeId
                }
        }

        fun toTicketEscDetailsModel(ticketEscDetailsDto: TicketEscDetailsDto): TicketEscDetails {
            return TicketEscDetails().apply {
                id = ticketEscDetailsDto.id
                ticket = ticketEscDetailsDto.ticket
                escalatorId = ticketEscDetailsDto.escalatorId
                escalateeId = ticketEscDetailsDto.escalateeId
            }
        }
    }
}