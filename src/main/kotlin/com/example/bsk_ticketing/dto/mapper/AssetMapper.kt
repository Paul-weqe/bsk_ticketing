package com.example.bsk_ticketing.dto.mapper

import com.example.bsk_ticketing.dto.model.AssetDto
import com.example.bsk_ticketing.model.Asset

class AssetMapper{
    companion object{

        fun toAssetDto(asset: Asset): AssetDto{
            return AssetDto(id=asset.id, name = asset.name, description = asset.description)
        }

        fun toAssetModel(assetDto: AssetDto): Asset{
            val asset = Asset()
            asset.id = assetDto.id
            asset.name = assetDto.name
            asset.description = assetDto.description
            return asset
        }

    }
}