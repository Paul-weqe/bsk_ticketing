package com.example.bsk_ticketing.dto.mapper

import com.example.bsk_ticketing.dto.model.UserDto
import com.example.bsk_ticketing.model.User

class UserMapper {
    companion object{
        fun toUserDto(user: User): UserDto{
            return UserDto(
                    id = user.id, firstName = user.firstName, lastName = user.lastName,
                    email = user.email, password = user.password, username = user.username,
                    passApproved = user.passApproved, role = user.role
            )
        }

        fun toUserModel(userDto: UserDto): User{
            val user = User()
            user.id = userDto.id
            user.firstName = userDto.firstName
            user.lastName = userDto.lastName
            user.email = userDto.email
            user.password = userDto.password
            user.username = userDto.username
            user.passApproved = userDto.passApproved
            user.role = userDto.role
            return user
        }

    }
}