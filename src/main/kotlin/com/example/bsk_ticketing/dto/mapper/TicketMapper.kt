package com.example.bsk_ticketing.dto.mapper

import com.example.bsk_ticketing.dto.model.TicketDto
import com.example.bsk_ticketing.model.Ticket

class TicketMapper{

    companion object{

        fun toTicketDto(ticket: Ticket): TicketDto{
            return TicketDto(
                    id = ticket.id, title = ticket.title, description = ticket.description, userId = ticket.userId,
                    userEmail = ticket.userEmail, userFirstName = ticket.userFirstName,
                    userLastName = ticket.userLastName, isOpen = ticket.isOpen, openedAt = ticket.openedAt,
                    closedAt = ticket.closedAt,closedByEmail = ticket.closedByEmail,
                    closedByFirstName = ticket.closedByFirstName, closedByLastName = ticket.closedByLastName,
                    closedById = ticket.closedById, assetId = ticket.assetId, assetName = ticket.assetName,
                    openedAtLessTime = ticket.createdOn, closedAtLessTime = ticket.resolvedAt, openedStatus = ticket.openedStatus,
                    engaged = ticket.engaged
            )
        }

        fun toTicketModel(ticketDto: TicketDto): Ticket{

            val ticket =  Ticket()
            ticket.id = ticketDto.id
            ticket.description = ticketDto.description
            ticket.title = ticketDto.title
            ticket.userId = ticketDto.userId
            ticket.userEmail = ticketDto.userEmail
            ticket.userFirstName = ticketDto.userFirstName
            ticket.userLastName = ticketDto.userLastName
            ticket.isOpen = ticketDto.isOpen
            ticket.openedAt = ticketDto.openedAt
            ticket.closedAt = ticketDto.closedAt
            ticket.closedByEmail = ticketDto.closedByEmail
            ticket.closedByFirstName = ticketDto.closedByFirstName
            ticket.closedByLastName = ticketDto.closedByLastName
            ticket.closedById = ticketDto.closedById
            ticket.assetId = ticketDto.assetId
            ticket.assetName = ticketDto.assetName
            ticket.createdOn = ticketDto.openedAtLessTime
            ticket.resolvedAt = ticketDto.closedAtLessTime
            ticket.openedStatus = ticketDto.openedStatus
            ticket.engaged = ticketDto.engaged
            return ticket

        }

    }
}