package com.example.bsk_ticketing.controller

import com.example.bsk_ticketing.dto.model.TicketDto
import com.example.bsk_ticketing.repository.UserRepository
import com.example.bsk_ticketing.service.TicketService
import com.example.bsk_ticketing.utils.PortalUtilities
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import lombok.extern.java.Log
import org.apache.commons.lang3.time.DateUtils
import org.jasypt.encryption.StringEncryptor
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.configurationprocessor.json.JSONObject
import org.springframework.data.domain.PageRequest
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import java.io.File
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.collections.ArrayList

@Log
@Controller
class ReportController(@Autowired userRepo: UserRepository, @Autowired jasyptStringEncryptor: StringEncryptor) {

    @Autowired val ticketService = TicketService()

    val portalUtilities = PortalUtilities(userRepo, jasyptStringEncryptor)

    @RequestMapping("/ticket/report")
//    @Secured(REPORTS)
    fun collectionsDisbursementsAction(model: Model, @RequestParam(defaultValue = "") reportType: String,
                                       @RequestParam(defaultValue = "") dateRange: String): String {

        if ("" != reportType && "" != dateRange) {
            val startDate = portalUtilities.getDateRangeValue(dateRange, 1)
            val endDate = portalUtilities.getDateRangeValue(dateRange, 2)
            var reportsFlag = true
            var reportDetails: ArrayList<TicketDto>? = null
            when (reportType) {
                "1" ->  {
                    reportsFlag = false
                    reportDetails = ticketService.downloadTicketReports(reportsFlag, DateUtils.truncate(startDate, Calendar.DATE), DateUtils.truncate(endDate, Calendar.DATE))
                }
                "2" -> reportDetails = ticketService.downloadTicketReports(reportsFlag, DateUtils.truncate(startDate, Calendar.DATE), DateUtils.truncate(endDate, Calendar.DATE))

                "3" -> reportDetails = ticketService.downloadTicketReportsWithoutStatus(DateUtils.truncate(startDate, Calendar.DATE), DateUtils.truncate(endDate, Calendar.DATE))
            }
            model.addAttribute("reportDetails", reportDetails)
            model.addAttribute("isGenerated", 1)
            val reportData = JSONObject()
            reportData.put("dateRange", dateRange)
            reportData.put("reportType", reportType)
            Logger.getLogger(javaClass.name).log(Level.INFO, "Report data: $reportData")
            model.addAttribute("generatedReport", portalUtilities.generateUniqueString(reportData.toString()))
        } else {
            model.addAttribute("isGenerated", 0)
            val startDate = Date.from(Instant.now().plusSeconds(86400))
            val endDate = Date.from(Instant.now().plusSeconds(86400))
            val reportDetails = ticketService.downloadTicketReportsWithoutStatus(startDate, endDate)
            model.addAttribute("reportDetails", reportDetails)
        }

        return "ticket-reports"
    }

    @RequestMapping("/ticket/report/download/{id}", method = [RequestMethod.GET])
//    @Secured(REPORTS)
    fun generateColDisFile(@PathVariable("id") uniqueID: String,
                           req: HttpServletRequest, resp: HttpServletResponse) {
        val mapper = ObjectMapper()

        try {
            val csvFilePath = PortalUtilities.getFilePath(req)
            Logger.getLogger(javaClass.name).log(Level.INFO, "CSV File path: $csvFilePath")
            Logger.getLogger(javaClass.name).log(Level.INFO, "Encrypted String Value: $uniqueID")
            Logger.getLogger(javaClass.name).log(Level.INFO, "Decrypted String Value: ${portalUtilities.getStringValue(uniqueID)}")
            val reportData = portalUtilities.getStringValue(uniqueID)
            val map = mapper.readValue<Map<String, Any>>(reportData, object : TypeReference<Map<String, String>>() {})
            val reportType = map["reportType"].toString()
            val dateRange = map["dateRange"].toString()
            val startDate = portalUtilities.getDateRangeValue(dateRange, 1)
            val endDate = portalUtilities.getDateRangeValue(dateRange, 2)
            Logger.getLogger(javaClass.name).log(Level.INFO, "Report type: $reportType, Start date: $startDate, End date: $endDate")
            var reportsFlag = true
            var reportDetails: ArrayList<TicketDto>? = ArrayList()

            when (reportType) {
                "1" ->  {
                    reportsFlag = false
                    reportDetails = ticketService.downloadTicketReports(reportsFlag, DateUtils.truncate(startDate, Calendar.DATE), DateUtils.truncate(endDate, Calendar.DATE))
                }
                "2" -> reportDetails = ticketService.downloadTicketReports(reportsFlag, DateUtils.truncate(startDate, Calendar.DATE), DateUtils.truncate(endDate, Calendar.DATE))

                "3" -> reportDetails = ticketService.downloadTicketReportsWithoutStatus(DateUtils.truncate(startDate, Calendar.DATE), DateUtils.truncate(endDate, Calendar.DATE))
            }

            Logger.getLogger(javaClass.name).log(Level.INFO, "BP 1")
            Logger.getLogger(javaClass.name).log(Level.INFO, "reportDetails: ${reportDetails}")
            val reportName = "NYA-SUPPORT-TICKETS-REPORT"
            val dataList = java.util.ArrayList<Map<String, Any>>()
            reportDetails!!.forEach { reportDetail ->
                val dataMap = LinkedHashMap<String, Any>()
                dataMap["ID"] = reportDetail.id
                dataMap["NAME"] = reportDetail.userFirstName + " " + reportDetail.userLastName
                dataMap["EMAIL"] = reportDetail.userEmail
                dataMap["TICKET TITLE"] = reportDetail.title
                dataMap["TICKET DESCRIPTION"] = reportDetail.description
                dataMap["OPEN/CLOSED"] = reportDetail.isOpen
                dataMap["OPENED AT"] = SimpleDateFormat("dd/MMM/yyyy HH:mm a").format(reportDetail.openedAt)
                dataMap["CLOSED BY"] = "N/A"
                dataMap["CLOSED AT"] = "N/A"
//                when(reportDetail.closedAt) {
//                    null -> {
//                        dataMap["CLOSED BY"] = "N/A"
//                        dataMap["CLOSED AT"] = "N/A"
//                    }
//                    else -> {
//                        dataMap["CLOSED BY"] = reportDetail.closedByFirstName + " " + reportDetail.closedByLastName
//                        dataMap["CLOSED AT"] = SimpleDateFormat("dd/MMM/yyyy HH:mm a").format(reportDetail.closedAt)
//                    }
//                }
                dataList.add(dataMap)
            }
            Logger.getLogger(javaClass.name).log(Level.INFO, "BP 2")
            Logger.getLogger(javaClass.name).log(Level.INFO, "dataList: ${dataList}")
            val finalCsvPath = portalUtilities.generateCSVFile(csvFilePath, reportName, dataList)
            Logger.getLogger(javaClass.name).log(Level.INFO, "finalCsvPath: ${finalCsvPath}")
            val downloadFile = File(finalCsvPath)
            if (downloadFile.exists()) {
                Logger.getLogger(javaClass.name).log(Level.INFO, "BP 3")
                PortalUtilities.downloadFileProperties(req, resp, finalCsvPath, downloadFile)
                Logger.getLogger(javaClass.name).log(Level.INFO, "BP 4")
            }
        } catch (ex: Exception) {
            Logger.getLogger(javaClass.name).log(Level.INFO, "EXCEPTION: $ex")
        }
    }
}