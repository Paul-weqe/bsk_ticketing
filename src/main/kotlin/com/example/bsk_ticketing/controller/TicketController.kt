package com.example.bsk_ticketing.controller

import com.example.bsk_ticketing.config.PagerModel
import com.example.bsk_ticketing.config.PortalConstants
import com.example.bsk_ticketing.dto.mapper.TicketMapper
import com.example.bsk_ticketing.dto.model.TicketDto
import com.example.bsk_ticketing.dto.model.TicketEscDetailsDto
import com.example.bsk_ticketing.exception.InvalidRoleException
import com.example.bsk_ticketing.exception.UserNotFoundException
import com.example.bsk_ticketing.model.TicketEscDetails
import com.example.bsk_ticketing.repository.UserRepository
import com.example.bsk_ticketing.service.AssetService
import com.example.bsk_ticketing.service.TicketEscalationService
import com.example.bsk_ticketing.service.TicketService
import com.example.bsk_ticketing.service.UserService
import com.example.bsk_ticketing.utils.PortalUtilities
import lombok.extern.java.Log
import org.jasypt.encryption.StringEncryptor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.util.StringUtils
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import java.security.Principal
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger
import java.util.stream.Collectors
import java.util.stream.IntStream
import javax.print.attribute.IntegerSyntax
import javax.servlet.http.HttpSession


@Log
@Controller
class TicketController(@Autowired userRepository: UserRepository, @Autowired jasyptStringEncryptor: StringEncryptor) {
    @Autowired val ticketService = TicketService()
    @Autowired val userService = UserService()
    @Autowired val assetService = AssetService()
    @Autowired val ticketEscalationService = TicketEscalationService()
    @Autowired val portalUtils = PortalUtilities(userRepository, jasyptStringEncryptor)

    @GetMapping("/create-ticket")
    @Secured(PortalConstants.PASS_APPROVED)
    fun getCreateTicket(model: Model, session: HttpSession): String{
        val flag: String
        return try {
            if (StringUtils.isEmpty(session.getAttribute("email")) ||
                    StringUtils.isEmpty(session.getAttribute("user_id")) ||
                    StringUtils.isEmpty(session.getAttribute("username"))) {
                throw UserNotFoundException("Please login before creating a ticket.")
            }
            flag = portalUtils.confirmPassword(session.getAttribute("email") as String, "create-ticket")
            val ticket = TicketDto()
            val allAssets = assetService.getAllAssets()
            model.addAttribute("userRole", session.getAttribute("role"))
            model.addAttribute("allAssets", allAssets)
            model.addAttribute("ticket", ticket)
            flag
        } catch (e: Exception) {
            Logger.getLogger(javaClass.name).log(Level.SEVERE, "getCreateTicket: Error[User is not logged in.]")
            return "redirect:/user-login"
        }
    }

    @PostMapping("/create-ticket")
    fun postCreateTicket(@ModelAttribute ticketDto: TicketDto, session: HttpSession, model: Model, redirectAttributes: RedirectAttributes): String{
        if (StringUtils.isEmpty(session.getAttribute("email")) ||
                StringUtils.isEmpty(session.getAttribute("user_id")) ||
                StringUtils.isEmpty(session.getAttribute("username"))) {
            Logger.getLogger(javaClass.name).log(Level.SEVERE, "getCreateTicket: Error[User is not logged in.]")
            throw UserNotFoundException("Please login before creating a ticket.")
        }
        val flag = portalUtils.confirmPassword(session.getAttribute("email") as String, "redirect:/user-tickets")
        val user = userService.getUserById(session.getAttribute("user_id") as Int)
        val ticket = ticketService.createTicket(user, ticketDto)
        redirectAttributes.addFlashAttribute("message", "Ticket created successfully.")
        model.addAttribute("assets", assetService.getAllAssets())
        return flag
    }

    @GetMapping("/user-tickets")
    @Secured(PortalConstants.PASS_APPROVED)
    fun getTickets( model: Model, session: HttpSession, @RequestParam("page") page: Optional<Int>): String{
        val flag: String
        return try {
            if (StringUtils.isEmpty(session.getAttribute("email")) ||
                    StringUtils.isEmpty(session.getAttribute("user_id")) ||
                    StringUtils.isEmpty(session.getAttribute("username")) ||
                    StringUtils.isEmpty(session.getAttribute("role"))) {
                throw UserNotFoundException("Please login before creating a ticket.")
            }
            flag = portalUtils.confirmPassword(session.getAttribute("email") as String, "user-tickets")

            Logger.getLogger(javaClass.name).log(Level.INFO, "user-tickets => flag: [${flag}]")

            val userTickets = ticketService.getAllTickets(PageRequest.of(page.orElse(1) - 1, PortalConstants.PAGE_SIZE))

            userTickets.forEach { ticket ->
                ticket.openedAtLessTime = SimpleDateFormat("dd/MM/yyy").format(ticket.openedAt)
                if (ticket.closedAt != null) ticket.closedAtLessTime = SimpleDateFormat("dd/MM/yyy").format(ticket.closedAt)
            }

            Logger.getLogger(javaClass.name).log(Level.INFO, "myTickets total elements: ${userTickets.totalElements}")
            model.addAttribute("userRole", session.getAttribute("role"))
            model.addAttribute("userTickets", userTickets)
            if(userTickets.totalPages > 0) {
                val pageNumbers: List<Int> = IntStream.rangeClosed(1, userTickets.totalPages).boxed().collect(Collectors.toList())
                model.addAttribute("pageNumbers", pageNumbers)
                model.addAttribute("pager", PagerModel(userTickets.totalPages, userTickets.number, 3))
                model.addAttribute("nextPageLink", "my-tickets")
            }
            model.addAttribute("userTickets", userTickets)
            model.addAttribute("userRole", session.getAttribute("role"))

//            Logger.getLogger(javaClass.name).log(Level.INFO, "Confirmed Password: [${user.passApproved}]")
            flag
        } catch (e: UserNotFoundException) {
            Logger.getLogger(javaClass.name).log(Level.SEVERE,
                    "getTickets: Error[User is not logged in.]")
            return "redirect:/user-login"
        } catch (e: InvalidRoleException) {
            Logger.getLogger(javaClass.name).log(Level.SEVERE,
                    "getTickets: User with ID [${session.getAttribute("user_id")}] Does Not Have A Valid Role")
            return "redirect:/user-login"
        }
    }

    @GetMapping("/user-tickets/{ticketId}")
    @Secured(PortalConstants.PASS_APPROVED)
    fun getSingleTicket(@PathVariable(value="ticketId") ticketId: String, model: Model, session: HttpSession): String{

        return try {
            val flag: String
            flag = portalUtils.confirmPassword(session.getAttribute("email") as String, "ticket-information")
            model.addAttribute("ticketescdetails", TicketEscDetailsDto())
            model.addAttribute("ticket", ticketService.getTicketById(Integer.valueOf(ticketId)))
            model.addAttribute("isOpen", ticketService.getTicketById(Integer.valueOf(ticketId)).isOpen)
            model.addAttribute("ticket_id", ticketService.getTicketById(Integer.valueOf(ticketId)).id)
            model.addAttribute("userRole", session.getAttribute("role"))
            model.addAttribute("userId", session.getAttribute("user_id"))
            Logger.getLogger(javaClass.name).log(Level.INFO, "Checking escalatees")
            model.addAttribute("escalatees", userService.getUserByRole(session))
            Logger.getLogger(javaClass.name).log(Level.INFO, "Checking escalatees")
//            model.addAttribute("escalator", session.getAttribute("user_id"))
            model.addAttribute("role", session.getAttribute("role"))
            model.addAttribute("comp_role", "ROLE_ADMIN")
            Logger.getLogger(javaClass.name).log(Level.INFO, "Got here here")
            flag
        } catch (e: Exception) {
            Logger.getLogger(javaClass.name).log(Level.SEVERE,
                    "getSingleTicket: Error[User is not logged in.]: Exception [${e}]")
            return "redirect:/user-login"
        }
    }

    @GetMapping("/close-ticket/{ticketId}")
    @Secured(PortalConstants.PASS_APPROVED)
    fun closeTicket(session: HttpSession, @PathVariable(value="ticketId") ticketId: Int, redirectAttributes: RedirectAttributes): String{
        return try {
        if (StringUtils.isEmpty(session.getAttribute("email"))) {return "redirect:/user-login"}
            val flag: String
            flag = portalUtils.confirmPassword(session.getAttribute("email") as String, "redirect:/user-tickets/")
            val ticket = ticketService.getTicketById(ticketId)
            val user = userService.getUserById(session.getAttribute("user_id") as Int)
            ticketService.closeTicket(user, ticket)
            flag
        } catch (e: Exception) {
            Logger.getLogger(javaClass.name).log(Level.SEVERE, "Close ticket: Exception [$e]")
            redirectAttributes.addFlashAttribute("message", "Ticket created successfully.")
            "redirect:/user-tickets"
        }
    }

    @GetMapping("/tickets/all-open-tickets")
    @Secured(PortalConstants.PASS_APPROVED)
    fun getAllOpenTickets(model: Model, session: HttpSession, principal: Principal, @RequestParam("page") page: Optional<Int>): String {
        val flag: String
        return try {
            val user = userService.getUserByUsername(principal.name)

            Logger.getLogger(javaClass.name).log(Level.INFO, "Principal name: [${principal.name}]")

            session.setAttribute("user_id", user!!.id)
            session.setAttribute("username", user.username)
            session.setAttribute("email", user.email)
            session.setAttribute("first_name", user.firstName)
            session.setAttribute("last_name", user.lastName)
            session.setAttribute("role", user.role)

            Logger.getLogger(javaClass.name).log(Level.INFO, "Username: [${user.username}]")
            Logger.getLogger(javaClass.name).log(Level.INFO, "Email: [${user.email}]")
            Logger.getLogger(javaClass.name).log(Level.INFO, "Role: [${session.getAttribute("role")}]")


            Logger.getLogger(javaClass.name).log(Level.INFO,"getAllOpenTickets: Executed till here")

            flag = portalUtils.confirmPassword(session.getAttribute("email") as String, "user-tickets")

            Logger.getLogger(javaClass.name).log(Level.INFO,"getAllOpenTickets: Flag [${flag}]")
            val openTickets = ticketService.getAllOpenorClosedTickets(PageRequest.of(page.orElse(1) - 1, PortalConstants.PAGE_SIZE), true)

            openTickets.forEach { ticket ->
                ticket.openedAtLessTime = SimpleDateFormat("dd/MM/yyy").format(ticket.openedAt)
                if (ticket.closedAt != null) ticket.closedAtLessTime = SimpleDateFormat("dd/MM/yyy").format(ticket.closedAt)
            }

            Logger.getLogger(javaClass.name).log(Level.INFO, "myTickets total elements: ${openTickets.totalElements}")
            model.addAttribute("userRole", session.getAttribute("role"))
            model.addAttribute("userTickets", openTickets)
            if(openTickets.totalPages > 0) {
                val pageNumbers: List<Int> = IntStream.rangeClosed(1, openTickets.totalPages).boxed().collect(Collectors.toList())
                model.addAttribute("pageNumbers", pageNumbers)
                model.addAttribute("pager", PagerModel(openTickets.totalPages, openTickets.number, 3))
                model.addAttribute("nextPageLink", "my-tickets")
            }
            model.addAttribute("userRole", session.getAttribute("role"))
            model.addAttribute("userTickets", openTickets)
            flag
        } catch (e: Exception) {
            e.printStackTrace()
            Logger.getLogger(javaClass.name).log(Level.SEVERE,
                    "getAllOpenTickets: Error[User is not logged in.]")
            return "redirect:/user-login"
        }
    }

    @GetMapping("/tickets/all-closed-tickets")
    @Secured(PortalConstants.PASS_APPROVED)
    fun getAllClosedTickets(model: Model, session: HttpSession, @RequestParam("page") page: Optional<Int>): String {
        val flag: String
        try {
            if (StringUtils.isEmpty(session.getAttribute("email")) ||
                    StringUtils.isEmpty(session.getAttribute("user_id")) ||
                    StringUtils.isEmpty(session.getAttribute("username"))) {
                throw UserNotFoundException("Please login before creating a ticket.")
            }
            flag = portalUtils.confirmPassword(session.getAttribute("email") as String, "user-tickets")
            val closedTickets = ticketService.getAllOpenorClosedTickets(PageRequest.of(page.orElse(1) - 1, PortalConstants.PAGE_SIZE),false)

            closedTickets.forEach { ticket ->
                ticket.openedAtLessTime = SimpleDateFormat("dd/MM/yyy").format(ticket.openedAt)
                if (ticket.closedAt != null) ticket.closedAtLessTime = SimpleDateFormat("dd/MM/yyy").format(ticket.closedAt)
            }

            Logger.getLogger(javaClass.name).log(Level.INFO, "myTickets total elements: ${closedTickets.totalElements}")
            model.addAttribute("userRole", session.getAttribute("role"))
            model.addAttribute("userTickets", closedTickets)
            if(closedTickets.totalPages > 0) {
                val pageNumbers: List<Int> = IntStream.rangeClosed(1, closedTickets.totalPages).boxed().collect(Collectors.toList())
                model.addAttribute("pageNumbers", pageNumbers)
                model.addAttribute("pager", PagerModel(closedTickets.totalPages, closedTickets.number, 3))
                model.addAttribute("nextPageLink", "my-tickets")
            }

            model.addAttribute("userRole", session.getAttribute("role"))
            model.addAttribute("userTickets", closedTickets)
        } catch (e: Exception) {
            Logger.getLogger(javaClass.name).log(Level.SEVERE,
                    "getAllClosedTickets: Error[User is not logged in.]")
            return "redirect:/user-login"
        }
        return flag
    }

    @PostMapping("/user-tickets/escalate")
    @Secured(PortalConstants.ROLE_SUPER_ADMIN, PortalConstants.PASS_APPROVED)
    fun escalateTicket(@ModelAttribute ticketEscDetails: TicketEscDetailsDto, model: Model): String {
        val escalatedTicket = ticketEscalationService.createTicketEscalation(ticketEscDetails)
        return "redirect:/user-tickets"
    }

    @GetMapping("/tickets/escalated-tickets")
    @Secured(PortalConstants.ROLE_SUPER_ADMIN, PortalConstants.PASS_APPROVED)
    fun getEscalatedTickets(model: Model, session: HttpSession): String {
        val flag: String = portalUtils.confirmPassword(session.getAttribute("email") as String, "user-tickets")
        val escalatedTickets = ticketService.getEscalatedTickets(session.getAttribute("user_id") as Int)
        model.addAttribute("userTickets", escalatedTickets)
        model.addAttribute("userRole", session.getAttribute("role") as Int)
        Logger.getLogger(javaClass.name).log(Level.INFO, "Escalated tickets: [${escalatedTickets}]")
        return flag
    }

    @GetMapping("/tickets/my-tickets")
    @Secured(PortalConstants.PASS_APPROVED)
    fun getUserTickets(model: Model, session: HttpSession, @RequestParam("page") page: Optional<Int>): String {
        val flag: String = portalUtils.confirmPassword(session.getAttribute("email") as String, "user-tickets")
        val myTickets = ticketService.getAllUserTicketsWithId(PageRequest.of(page.orElse(1) - 1, PortalConstants.PAGE_SIZE), session.getAttribute("user_id") as Int)
        Logger.getLogger(javaClass.name).log(Level.INFO, "myTickets total elements: ${myTickets.totalElements}")
        myTickets.forEach { ticket ->
            ticket.openedAtLessTime = SimpleDateFormat("dd/MM/yyy").format(ticket.openedAt)
            if (ticket.closedAt != null) ticket.closedAtLessTime = SimpleDateFormat("dd/MM/yyy").format(ticket.closedAt)
        }
        model.addAttribute("userRole", session.getAttribute("role"))
        model.addAttribute("userTickets", myTickets)
        if(myTickets.totalPages > 0) {
            val pageNumbers: List<Int> = IntStream.rangeClosed(1, myTickets.totalPages).boxed().collect(Collectors.toList())
            model.addAttribute("pageNumbers", pageNumbers)
            model.addAttribute("pager", PagerModel(myTickets.totalPages, myTickets.number, 3))
            model.addAttribute("nextPageLink", "my-tickets")
        }
        return flag
    }

    @GetMapping("/ticket/resolve-ticket/{ticketId}")
    @Secured(PortalConstants.PASS_APPROVED)
    fun resolveTicket(@PathVariable(value="ticketId") ticketId: String, model: Model, session: HttpSession, principal: Principal): String{
        val user = userService.getUserByUsername(principal.name)
        val ticket = ticketService.getTicketById(Integer.valueOf(ticketId))
        ticket.closedByFirstName = user!!.firstName
        Logger.getLogger(javaClass.name).log(Level.INFO, "closedByFirstName: ${ticket.closedByFirstName}")
        ticket.closedByLastName = user.lastName
        ticket.engaged = 1
        ticketService.ticketRepository.save(TicketMapper.toTicketModel(ticket))
        return "redirect:/user-tickets/$ticketId"
    }
}
