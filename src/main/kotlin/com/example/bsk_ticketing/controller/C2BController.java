package com.example.bsk_ticketing.controller;

import com.example.bsk_ticketing.integ.C2BResponse;
import com.example.bsk_ticketing.integ.Response;
import com.example.bsk_ticketing.service.C2BService;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Level;

@Log
@RestController
public class C2BController {

    private final C2BService c2BService;
    private final Response response;

    public C2BController(C2BService c2BService, Response response) {
        this.c2BService = c2BService;
        this.response = response;
    }

    @GetMapping("/c2b/response")
    public Response getC2BResponse(@RequestParam String resp) {
        log.log(Level.INFO, resp);
        try {
            XmlMapper mapper = new XmlMapper();
            C2BResponse c2BResponse = mapper.readValue(resp, C2BResponse.class);
            log.log(Level.INFO, c2BResponse.toString());
//            c2BService.sendC2BResponse(c2BResponse);
            response.setStatus("200");
            response.setMessage("C2B response received successfully.");

        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
            response.setStatus("500");
            response.setMessage("Error occured. Contact admin for assistance");
        }
        return response;
    }
}

