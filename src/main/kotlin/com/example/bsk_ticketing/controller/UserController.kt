package com.example.bsk_ticketing.controller

import com.example.bsk_ticketing.config.PortalConstants
import com.example.bsk_ticketing.dto.model.UserDto
import com.example.bsk_ticketing.exception.InvalidRoleException
import com.example.bsk_ticketing.exception.UserNotFoundException
import com.example.bsk_ticketing.model.User
import com.example.bsk_ticketing.repository.UserRepository
import com.example.bsk_ticketing.service.UserService
import com.example.bsk_ticketing.utils.PortalUtilities
import org.jasypt.encryption.StringEncryptor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import java.util.logging.Level
import java.util.logging.Logger
import javax.servlet.http.HttpSession

@Controller
class UserController(@Autowired userRepository: UserRepository, @Autowired jasyptStringEncryptor: StringEncryptor) {
    @Autowired
    val userService = UserService()
    val bCryptPasswordEncoder = BCryptPasswordEncoder()
    val portalUtils = PortalUtilities(userRepository, jasyptStringEncryptor)

    @GetMapping("/sign-up")
    @Secured(PortalConstants.ROLE_ADMIN)
//    @PreAuthorize("hasRole('ROLE_ADMIN') and hasRole('ROLE_PASS_CONFIRMED')")
    fun getSignUp(httpSession: HttpSession, model: Model): String{
        val auth: Authentication = SecurityContextHolder.getContext().authentication
//        if (auth !is AnonymousAuthenticationToken) return "redirect:/user-tickets"
        model.addAttribute("user", UserDto())
        model.addAttribute("userRole", httpSession.getAttribute("role"))
//        return portalUtils.confirmPassword(httpSession.getAttribute("email") as String, "sign-up")
        return "sign-up"
    }

    @PostMapping("/sign-up")
//    @PreAuthorize("hasRole('ROLE_ADMIN') and hasRole('ROLE_PASS_CONFIRMED')")
    fun postSignUp(@ModelAttribute userDto: UserDto, session: HttpSession, redirectAttributes: RedirectAttributes): String{

        return try{ userService.signUp(userDto)
            portalUtils.confirmPassword(session.getAttribute("email") as String, "redirect:/user-tickets")
        }
        catch(e: Exception){
            redirectAttributes.addFlashAttribute("message", "The username you provided already exists.")
            return "redirect:/sign-up"
        }
    }

    @GetMapping("", "/", "/user-login")
    fun getLogin(model: Model): String{
        val auth: Authentication = SecurityContextHolder.getContext().authentication
        if (auth !is AnonymousAuthenticationToken) return "redirect:/user-tickets"
        model.addAttribute("user", UserDto())
        return "user-login"
    }

    @PostMapping("/user-login")
    fun postLogin(
            @ModelAttribute userDto: UserDto, session: HttpSession, redirectAttributes: RedirectAttributes
    ): String {
        var flag = "redirect:/user-tickets"
        return try {
            userService.loginUser(userDto.email, userDto.password).apply {
                session.setAttribute("user_id", this.id)
                session.setAttribute("username", this.username)
                session.setAttribute("email", this.email)
                session.setAttribute("first_name", this.firstName)
                session.setAttribute("last_name", this.lastName)
                session.setAttribute("role", this.role)
                session.setAttribute("confirmedPassword", this.passApproved)

                Logger.getLogger(javaClass.name).log(Level.INFO, "Confrimed Password: [${this.passApproved}]")
                if (this.passApproved == "0" || this.passApproved!!.isEmpty() || this.passApproved == "" || this.passApproved == null) {
                    Logger.getLogger(javaClass.name).log(Level.INFO, "Never came here")
                    flag = "redirect:/change-password"
                }
            }
            flag
        } catch (e: UserNotFoundException) {
            redirectAttributes.addFlashAttribute("message", "The credentials provided do not match. Please try again.")
            "redirect:/user-login"
        }
    }

    @GetMapping("/change-password")
    fun changeUserPassword(model: Model): String{
        model.addAttribute("user", User())
        return "change-password"
    }

    @PostMapping("/change-password")
    @Secured(PortalConstants.ROLE_ADMIN)
    fun changeUserPassword(
            @ModelAttribute user: User, session: HttpSession, redirectAttributes: RedirectAttributes
    ): String {
        return try {
            user.password = bCryptPasswordEncoder.encode(user.password)
            userService.changeUserPassword(session.getAttribute("username") as String, user)
            "redirect:/tickets/all-open-tickets"
        } catch (e: Exception) {
            redirectAttributes.addFlashAttribute("message", e.message)
            "redirect:/change-password"
        }
    }

    @GetMapping("/user-logout")
    @Secured(PortalConstants.ROLE_ADMIN, PortalConstants.ROLE_SUPER_ADMIN, PortalConstants.ROLE_USER)
    fun logOut(session: HttpSession): String {
        session.invalidate()
        return "redirect:/user-login"
    }

    @GetMapping("/access-denied")
    fun accessDenied(): String {
        Logger.getLogger(javaClass.name).log(Level.INFO, "access denied for user")
        return "access-denied"
    }
}