package com.example.bsk_ticketing.controller

import com.example.bsk_ticketing.dto.model.AssetDto
import com.example.bsk_ticketing.service.AssetService
import com.example.bsk_ticketing.service.TicketService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping

@Controller
class AssetController {

    @Autowired
    val assetService = AssetService()

    @Autowired
    val ticketService = TicketService()

    @GetMapping("/create-asset")
    fun getCreateAsset(
            model: Model
    ): String{
        model.addAttribute("asset", AssetDto())
        return "create-asset"
    }

    @PostMapping("/create-asset")
    fun postCreateAsset(
            @ModelAttribute assetDto: AssetDto
    ): String{
        assetService.createAsset(assetDto)
        return "redirect:/user-tickets"
    }

    @GetMapping("/view-assets")
    fun getAllAssets(
            model: Model
    ): String{
        val allAssets = assetService.getAllAssets()
        model.addAttribute("allAssets", allAssets)
        return "view-assets"
    }

    @GetMapping("view-asset/{assetId}")
    fun getSingleAsset(
            @PathVariable(value="assetId") assetId: Int, model: Model
    ): String{
        val asset = assetService.getAssetById(assetId)
        val assetTickets = ticketService.getAllTicketsByAsset(assetId)
        model.addAttribute("asset", asset)
        model.addAttribute("assetTickets", assetTickets)
        return "asset-information"
    }

}
