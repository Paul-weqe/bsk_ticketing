package com.example.bsk_ticketing.integ;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
        * <?xml version="1.0" encoding="UTF-8"?>
        * <COMMAND>
        * <STATUS></STATUS>
        * <MESSAGE></MESSAGE>
        * <COMMAND>
 */

@Data
@JacksonXmlRootElement(localName = "COMMAND")
@JsonPropertyOrder({"STATUS","TXNID","MESSAGE"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@Component
@Setter
public class Response {
    @JsonProperty("STATUS")
    protected String status;

    @JsonProperty("MESSAGE")
    protected String message;
}
