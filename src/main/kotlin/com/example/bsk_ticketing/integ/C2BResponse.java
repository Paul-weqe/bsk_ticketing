package com.example.bsk_ticketing.integ;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

/**
 * <?xml version="1.0" encoding="UTF-8"?>
 * <COMMAND>
 * <STATUS></STATUS>
 * <TXNID></TXNID>
 * <MESSAGE></MESSAGE>
 * <COMMAND>
 */

@Data
@JacksonXmlRootElement(localName = "COMMAND")
@JsonPropertyOrder({"STATUS","TXNID","MESSAGE"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class C2BResponse {
    @JsonProperty("STATUS")
    protected String status;

    @JsonProperty("TXNID")
    protected String txnId;

    @JsonProperty("MESSAGE")
    protected String message;
}

