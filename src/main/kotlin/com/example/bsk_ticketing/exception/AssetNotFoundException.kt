package com.example.bsk_ticketing.exception

class AssetNotFoundException(message: String) : Exception(message)