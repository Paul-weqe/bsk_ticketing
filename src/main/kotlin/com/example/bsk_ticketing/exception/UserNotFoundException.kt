package com.example.bsk_ticketing.exception

class UserNotFoundException(message: String) : Exception(message)