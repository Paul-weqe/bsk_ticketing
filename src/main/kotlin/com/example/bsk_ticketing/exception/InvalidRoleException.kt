package com.example.bsk_ticketing.exception

class InvalidRoleException(message: String): Exception(message)