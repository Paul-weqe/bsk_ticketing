package com.example.bsk_ticketing.exception

class TicketNotFoundException(message: String): Exception(message)