package com.example.bsk_ticketing.utils

import com.example.bsk_ticketing.repository.UserRepository
import org.jasypt.encryption.StringEncryptor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import java.io.*
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Service
class PortalUtilities(val userRepository: UserRepository, val jasyptStringEncryptor: StringEncryptor) {

    fun confirmPassword(email: String, defaultPath: String): String {
        val user = userRepository.findByEmail(email)
        Logger.getLogger(javaClass.name).log(Level.INFO, "Got here")
        Logger.getLogger(javaClass.name).log(Level.INFO, "Email: $email")
        Logger.getLogger(javaClass.name).log(Level.INFO, "Username: ${user!!.username}")
        Logger.getLogger(javaClass.name).log(Level.INFO, "Confirm Password: ${user.passApproved}")

        var flag = defaultPath
        Logger.getLogger(javaClass.name).log(Level.INFO, "Break point")
        if (user.passApproved == null || user.passApproved == "0" || user.passApproved == "PLACEHOLDER" || user.passApproved!!.isEmpty() || user.passApproved!!.isBlank()) {
            flag = "redirect:/change-password"
        }
        Logger.getLogger(javaClass.name).log(Level.INFO, "Got here here here here")
        return flag
    }

    fun getDateRangeValue(dateRange: String, type: Int?): Date? {
        var mydate: Date? = null
        dateRange.replace(":".toRegex(), "/")
        val rangeParts = dateRange.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val startDate = rangeParts[0]
        val endDate = rangeParts[1]
        try {
            if (type == 1) {
                mydate = SimpleDateFormat("dd/MM/yyyy").parse(startDate)
            } else {
                mydate = SimpleDateFormat("dd/MM/yyyy").parse(endDate)
            }
        } catch (e: java.text.ParseException) {

        }

        return mydate
    }

    fun generateUniqueString(strValue: String): String? {
        var uniqueString = ""
        var corectFormat = false
        try {
            while (corectFormat == false) {
                uniqueString = jasyptStringEncryptor.encrypt(strValue)
                if (!uniqueString.contains("/")) {
                    corectFormat = true
                }
            }
        } catch (e: Exception) {
            Logger.getLogger(javaClass.name).log(Level.INFO, "EXCEPTION: $e")
        }

        return uniqueString
    }

    fun getStringValue(strEncrypted: String): String {
        var strValue = ""
        try {
            strValue = jasyptStringEncryptor.decrypt(strEncrypted)
        } catch (e: Exception) {
            strValue = ""
            Logger.getLogger(javaClass.name).log(Level.INFO, "Get String Value: $e")
        }

        return strValue
    }

    fun generateCSVFile(filePath: String, generatedContentName: String, dataList: ArrayList<Map<String, Any>>): String {
        val instant = Instant.now()
        val csvFolder = File(filePath)
        if (!csvFolder.exists()) {
            csvFolder.mkdirs()
            csvFolder.setWritable(true)
        }

        val generatedCsvFilePath = filePath + generatedContentName + "-" + instant.epochSecond + ".csv"
        val currentFile = File(generatedCsvFilePath)

        dataList.forEach { requestMap ->

            //Setting the Headers
            if (!currentFile.exists()) {
                try {
                    FileWriter(currentFile, true).use { fileWriter ->
                        val headersStringBuilder = StringBuilder()
                        requestMap.keys.stream().map { keys ->
                            headersStringBuilder.append(keys)
                            keys
                        }.forEachOrdered { _item -> headersStringBuilder.append(",") }
                        headersStringBuilder.append(System.lineSeparator())
                        fileWriter.append(headersStringBuilder.toString())
                    }
                } catch (e: IOException) {
                    Logger.getLogger(javaClass.name).log(Level.INFO, "Generate File Exception: $e")
                }

            }
            //Populating the data
            try {
                FileWriter(currentFile, true).use { fileWriter ->
                    val bodyBuilder = StringBuilder()
                    requestMap.keys.stream().map { keys ->
                        bodyBuilder.append(requestMap[keys])
                        keys
                    }.forEachOrdered { _item -> bodyBuilder.append(",") }
                    bodyBuilder.append(System.lineSeparator())
                    if (!currentFile.canWrite()) {
                        currentFile.setWritable(true)
                    }
                    fileWriter.append(bodyBuilder.toString())
                }
            } catch (e: IOException) {
                Logger.getLogger(javaClass.name).log(Level.INFO, "Populating data Exception: $e")
            }
        }

        return generatedCsvFilePath
    }

    companion object {
        @Throws(FileNotFoundException::class)
        fun getFilePath(req: HttpServletRequest): String {
            var appPath = ""
            var fullPath = ""
            appPath = req.session.servletContext.getRealPath("")
            fullPath = appPath
            return fullPath
        }

        fun downloadFileProperties(req: HttpServletRequest, resp: HttpServletResponse, toBeDownloadedFile: String, downloadFile: File) {
            try {
                var mimeType: String? = req.session.servletContext.getMimeType(toBeDownloadedFile)
                if (mimeType == null) {
                    mimeType = "application/octet-stream"
                }
                resp.contentType = mimeType
                resp.setContentLength(downloadFile.length().toInt())
                val headerKey = "Content-Disposition"
                val headerValue = String.format("attachment; filename=\"%s\"", downloadFile.name)
                resp.setHeader(headerKey, headerValue)
                val outStream = resp.outputStream
                val inputStream = FileInputStream(downloadFile)
                val buffer = ByteArray(4096)
                var bytesRead = -1
                while ((inputStream.read(buffer)) != -1) {
                    bytesRead = inputStream.read(buffer)
                    outStream.write(buffer, 0, bytesRead)
                }
                inputStream.close()
                outStream.close()
            } catch (ioExObj: IOException) {
                Logger.getLogger(javaClass.name).log(Level.INFO, "EXCEPTION: $ioExObj")
            }
        }
    }
}
