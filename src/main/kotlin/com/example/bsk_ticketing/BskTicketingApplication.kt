package com.example.bsk_ticketing

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer

@SpringBootApplication
class BskTicketingApplication: SpringBootServletInitializer()

fun main(args: Array<String>) {
    runApplication<BskTicketingApplication>(*args)
}
