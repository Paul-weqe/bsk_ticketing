package com.example.bsk_ticketing.service

import com.example.bsk_ticketing.dto.model.UserDto
import com.example.bsk_ticketing.model.User
import javax.servlet.http.HttpSession

interface IUserService {
    fun signUp(userDto: UserDto): UserDto
    fun loginUser(email: String, password: String): UserDto
    fun changeUserPassword(username: String, user: User): UserDto
    fun getUserById(userId: Int): UserDto
    fun getUserByRole(session: HttpSession): ArrayList<UserDto>
    fun getDummyUserByRole(role: Int): ArrayList<UserDto>
    fun getUserByUsername(username: String): User?
}