package com.example.bsk_ticketing.service

import com.example.bsk_ticketing.dto.model.TicketEscDetailsDto
import com.example.bsk_ticketing.model.TicketEscDetails

interface ITicketEscalationService {
    fun createTicketEscalation(ticket: TicketEscDetailsDto): TicketEscDetails
}