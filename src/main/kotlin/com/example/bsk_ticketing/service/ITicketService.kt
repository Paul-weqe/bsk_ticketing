package com.example.bsk_ticketing.service

import com.example.bsk_ticketing.dto.model.TicketDto
import com.example.bsk_ticketing.dto.model.UserDto
import com.example.bsk_ticketing.model.Ticket
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import java.util.*

interface ITicketService {
    fun createTicket(user: UserDto, ticketDto: TicketDto): TicketDto
    fun closeTicket(user: UserDto, ticketDto: TicketDto): TicketDto
    fun getAllUserTickets(pageable: Pageable, userId: Int): Page<TicketDto>
    fun getTicketById(ticketId: Int): TicketDto
    fun getAllTicketsByAsset(assetId: Int): ArrayList<TicketDto>
    fun getAllOpenorClosedTickets(pageable: Pageable, isOpen: Boolean): Page<TicketDto>
    fun getAllUserTicketsAndIsOpenOrIsClosed(pageable: Pageable, userId: Int, isOpen: Boolean): Page<TicketDto>
    fun getEscalatedTickets(userId: Int): ArrayList<TicketDto>
    fun getAllTickets(pageable: Pageable): Page<TicketDto>
    fun downloadTicketReports(isOpen: Boolean, startDate: Date, endDate: Date): ArrayList<TicketDto>
    fun downloadTicketReportsWithoutStatus(startDate: Date, endDate: Date): ArrayList<TicketDto>
    fun getAllUserTicketsWithId(pageable: Pageable, userId: Int): Page<TicketDto>
}