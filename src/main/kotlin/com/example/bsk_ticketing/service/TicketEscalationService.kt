package com.example.bsk_ticketing.service

import com.example.bsk_ticketing.dto.mapper.TicketEscDetailsMapper
import com.example.bsk_ticketing.dto.model.TicketEscDetailsDto
import com.example.bsk_ticketing.model.TicketEscDetails
import com.example.bsk_ticketing.repository.TicketEscalationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TicketEscalationService: ITicketEscalationService {

    @Autowired
    lateinit var ticketEscalationRepository: TicketEscalationRepository
    override fun createTicketEscalation(ticket: TicketEscDetailsDto): TicketEscDetails {
        return ticketEscalationRepository.save(TicketEscDetailsMapper.toTicketEscDetailsModel(ticket))
    }
}