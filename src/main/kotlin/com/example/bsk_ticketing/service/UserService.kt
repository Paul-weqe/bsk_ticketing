package com.example.bsk_ticketing.service

import com.example.bsk_ticketing.dto.mapper.UserMapper
import com.example.bsk_ticketing.dto.model.UserDto
import com.example.bsk_ticketing.exception.UserNotFoundException
import com.example.bsk_ticketing.model.User
import com.example.bsk_ticketing.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.lang.NullPointerException
import java.util.logging.Level
import java.util.logging.Logger
import javax.servlet.http.HttpSession

@Service
class UserService : IUserService{

    @Autowired
    lateinit var userRepository: UserRepository

    override fun signUp(userDto: UserDto): UserDto {
        userRepository.findByUsername(userDto.username)?.let {
            throw UserNotFoundException("User already found in the system")
        }?: kotlin.run {
            Logger.getLogger(javaClass.name).log(Level.INFO, "Came here instead")
            userDto.password = BCryptPasswordEncoder().encode(userDto.password)
            val user = UserMapper.toUserModel(userDto)
            return UserMapper.toUserDto(userRepository.save(user))
        }
    }

    override fun loginUser(email: String, password: String): UserDto {
        val user: User? = userRepository.findByEmailAndPassword(email, password) ?: throw UserNotFoundException("user with those credentials could not be found in the system")
        return UserMapper.toUserDto(user!!)
    }

    override fun getUserById(userId: Int): UserDto {
        val user = userRepository.findById(userId)
//        if (user.isEmpty) throw UserNotFoundException("user with ID {} could not be found")
        return UserMapper.toUserDto(user.get())
    }

    override fun getUserByRole(session: HttpSession): ArrayList<UserDto> {

        val userDtoList = ArrayList<UserDto>()

        when(session.getAttribute("role")) {
            "ROLE_USER" -> ArrayList<UserDto>()
            "ROLE_ADMIN_STUB" -> userRepository.findAllByRole("ROLE_ADMIN").forEach {
                userDtoList.add(UserMapper.toUserDto(it))
            }
            "ROLE_ADMIN" -> {
                userRepository.findAllByRole("ROLE_ADMIN").forEach{
                    userDtoList.add(UserMapper.toUserDto(it))
                }
                userDtoList.remove(UserMapper.toUserDto(userRepository.findById(session.getAttribute("user_id") as Int).get()))
            }
            else -> throw UserNotFoundException("Could not get escalatees related to role [${session.getAttribute("role")}]")
        }

        return userDtoList
    }

    override fun changeUserPassword(username: String, user: User): UserDto {
         userRepository.findByUsername(username)
                 .let {

                         it!!.password = user.password
                         it.passApproved = "ROLE_PASS_CONFIRMED"
                         return UserMapper.toUserDto(userRepository.save(it))
                 }
    }


    override fun getDummyUserByRole(role: Int): ArrayList<UserDto>   {

        val userDtoList = ArrayList<UserDto>()

        when(role){
            1 -> userRepository.findAllByRole("ROLE_ADMIN").forEach {
                userDtoList.add(UserMapper.toUserDto(it))
            }
            2 -> {
                userRepository.findAllByRoleOrRole("ROLE_USER", "ROLE_ADMIN").forEach{
                    userDtoList.add(UserMapper.toUserDto(it))
                }
                userDtoList.remove(UserMapper.toUserDto(userRepository.findById(26).get()))
            }
            else -> throw UserNotFoundException("Could not get escalatees related to role [${role}]")
        }

        return userDtoList
    }

    override fun getUserByUsername(username: String): User? {
        return userRepository.findByUsername(username)?.let {
            it
        }?: run {
            throw UserNotFoundException("User with username: ${username} cannot be found in our local store.")
        }
    }
}
