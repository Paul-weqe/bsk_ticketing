package com.example.bsk_ticketing.service

import com.example.bsk_ticketing.dto.mapper.TicketMapper
import com.example.bsk_ticketing.dto.model.TicketDto
import com.example.bsk_ticketing.dto.model.UserDto
import com.example.bsk_ticketing.repository.TicketEscalationRepository
import com.example.bsk_ticketing.repository.TicketRepository
import com.example.bsk_ticketing.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.sql.Timestamp
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger
import kotlin.collections.ArrayList

@Service
class TicketService: ITicketService {

    @Autowired
    lateinit var ticketRepository: TicketRepository

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var ticketEscRepository: TicketEscalationRepository

    override fun createTicket(user: UserDto, ticketDto: TicketDto): TicketDto {
        val foundUser = userRepository.findById(user.id)

//        if (foundUser .isEmpty) throw UserNotFoundException("User creating ticket does not exist in the system")
        ticketDto.apply {
            this.userEmail = foundUser.get().email
            this.userFirstName = foundUser.get().firstName
            this.userLastName = foundUser.get().lastName
            this.userId = foundUser.get().id
            this.userEmail = foundUser.get().email
        }
        val ticket = ticketRepository.save(TicketMapper.toTicketModel(ticketDto))
        return TicketMapper.toTicketDto(ticket)
    }

    override fun closeTicket(user: UserDto, ticketDto: TicketDto): TicketDto {
        ticketDto.apply {
            this.closedAt = Timestamp(System.currentTimeMillis())
            this.closedByEmail = user.email
            this.closedByFirstName = user.firstName
            this.closedByLastName = user.lastName
            this.closedById = user.id
            this.openedStatus = 0
            this.engaged = 2
            this.isOpen = false
        }
        val ticket = ticketRepository.save(TicketMapper.toTicketModel(ticketDto))
        return TicketMapper.toTicketDto(ticket)
    }

    //todo: refactor to get specific user id tickets
    override fun getAllUserTickets(pageable: Pageable, userId: Int): Page<TicketDto> {
        val size = pageable.pageSize
        val currentPage = pageable.pageNumber
        Logger.getLogger(javaClass.name).log(Level.INFO, "page size: ${size}")
        val startItem = size * currentPage
        val ticketsPage = ticketRepository.findAllByIsOpen(pageable, true)
        Logger.getLogger(javaClass.name).log(Level.INFO, "pages: ${ticketsPage.totalPages}")
        Logger.getLogger(javaClass.name).log(Level.INFO, "total no of elements: ${ticketsPage.totalElements}")
        return when (ticketsPage.size < 0) {
            true -> {
                Logger.getLogger(javaClass.name).log(Level.INFO, "came here")
                val ticketDtoList: List<TicketDto> = Collections.emptyList()
                PageImpl<TicketDto>(ticketDtoList,
                PageRequest.of(currentPage, size), ticketsPage.size.toLong())}
            else -> {
                Logger.getLogger(javaClass.name).log(Level.INFO, "otherwise came here")
                val toIndex = Math.min(startItem + size, ticketsPage.size)
                val ticketsDto = ArrayList<TicketDto>()
                ticketsPage.forEach {
                    ticketsDto.add(TicketMapper.toTicketDto(it))
                }
                PageImpl<TicketDto>(ticketsDto,
                        PageRequest.of(currentPage, size), ticketsPage.totalElements)
            }
        }
    }

    override fun getTicketById(ticketId: Int): TicketDto {
        val ticket = ticketRepository.findById(ticketId)
        Logger.getLogger(javaClass.name).log(Level.INFO, "Ticket: [$ticket]")
//        if (ticket.isEmpty) throw TicketNotFoundException("ticket with ID $ticketId could not be found")
        Logger.getLogger(javaClass.name).log(Level.INFO, "Ticket isOpen: [${TicketMapper.toTicketDto(ticket.get()).isOpen}]")
        return TicketMapper.toTicketDto(ticket.get())
    }

    override fun getAllTicketsByAsset(assetId: Int): ArrayList<TicketDto> {
        val tickets = ticketRepository.findAllByAssetId(assetId)
        val ticketsDto = ArrayList<TicketDto>()
        tickets.forEach { ticketsDto.add(TicketMapper.toTicketDto(it)) }
        return ticketsDto
    }

    //removed when clause ? userId == null
    override fun getAllOpenorClosedTickets(pageable: Pageable, isOpen: Boolean): Page<TicketDto> {
        Logger.getLogger(javaClass.name).log(Level.INFO, "BP 1")
        val tickets = ticketRepository.findAllByIsOpen(pageable, isOpen)
        Logger.getLogger(javaClass.name).log(Level.INFO, "BP 2")
        val ticketsDto = ArrayList<TicketDto>()
        Logger.getLogger(javaClass.name).log(Level.INFO, "BP 3")
        tickets.forEach { ticketsDto.add(TicketMapper.toTicketDto(it)) }
        Logger.getLogger(javaClass.name).log(Level.INFO, "BP 4")
        return PageImpl<TicketDto>(ticketsDto, PageRequest.of(pageable.pageNumber, pageable.pageSize), tickets.totalElements)
    }

    //todo: integrate changes with controller
    override fun getAllUserTicketsAndIsOpenOrIsClosed(pageable: Pageable, userId: Int, isOpen: Boolean): Page<TicketDto> {
        val tickets = ticketRepository.findAllByUserIdAndIsOpen(pageable,userId, isOpen)
        val ticketsDto = ArrayList<TicketDto>()
        tickets.forEach{ ticketsDto.add(TicketMapper.toTicketDto(it))}
        return PageImpl<TicketDto>(ticketsDto, PageRequest.of(pageable.pageNumber, pageable.pageSize), tickets.totalElements)
    }

    override fun getEscalatedTickets(userId: Int): ArrayList<TicketDto> {
        val tickets = ArrayList<TicketDto>()
        val escalatedTickets = ticketEscRepository.findAllByEscalateeId(userId)
        escalatedTickets.forEach {
            if (ticketRepository.findById(it.ticket.id).get().isOpen) tickets.add(TicketMapper.toTicketDto(ticketRepository.findById(it.ticket.id).get()))
        }
        return tickets
    }

    override fun getAllTickets(pageable: Pageable): Page<TicketDto> {
        val tickets = ArrayList<TicketDto>()
        val allTickets = ticketRepository.findAll(pageable)
        allTickets.forEach {
            tickets.add(TicketMapper.toTicketDto(it))
        }
        return PageImpl<TicketDto>(tickets,
                PageRequest.of(pageable.pageNumber, pageable.pageSize), allTickets.totalElements)
    }

    override fun downloadTicketReports(isOpen: Boolean, startDate: Date, endDate: Date): ArrayList<TicketDto> {
        val tickets = ArrayList<TicketDto>()
        val allTickets = ticketRepository.findAllByIsOpenAndOpenedAtGreaterThanEqualAndOpenedAtLessThanEqual(isOpen, startDate, endDate)
        allTickets.forEach {
            tickets.add(TicketMapper.toTicketDto(it))
        }
        return tickets
    }

    override fun downloadTicketReportsWithoutStatus(startDate: Date, endDate: Date): ArrayList<TicketDto> {
        val tickets = ArrayList<TicketDto>()
        val allTickets = ticketRepository.findAllByOpenedAtGreaterThanEqualAndOpenedAtLessThanEqual(startDate, endDate)
        allTickets.forEach {
            tickets.add(TicketMapper.toTicketDto(it))
        }
        return tickets
    }

    override fun getAllUserTicketsWithId(pageable: Pageable, userId: Int): Page<TicketDto> {
        val size = pageable.pageSize
        val currentPage = pageable.pageNumber
        Logger.getLogger(javaClass.name).log(Level.INFO, "page size: ${size}")
        val startItem = size * currentPage
        val ticketsPage = ticketRepository.findAllByUserId(pageable, userId)
        Logger.getLogger(javaClass.name).log(Level.INFO, "pages: ${ticketsPage.totalPages}")
        Logger.getLogger(javaClass.name).log(Level.INFO, "total no of elements: ${ticketsPage.totalElements}")
        return when (ticketsPage.size < 0) {
            true -> {
                Logger.getLogger(javaClass.name).log(Level.INFO, "came here")
                val ticketDtoList: List<TicketDto> = Collections.emptyList()
                PageImpl<TicketDto>(ticketDtoList,
                        PageRequest.of(currentPage, size), ticketsPage.size.toLong())}
            else -> {
                Logger.getLogger(javaClass.name).log(Level.INFO, "otherwise came here")
                val toIndex = Math.min(startItem + size, ticketsPage.size)
                val ticketsDto = ArrayList<TicketDto>()
                ticketsPage.forEach {
                    ticketsDto.add(TicketMapper.toTicketDto(it))
                }
                PageImpl<TicketDto>(ticketsDto,
                        PageRequest.of(currentPage, size), ticketsPage.totalElements)
            }
        }
    }
}