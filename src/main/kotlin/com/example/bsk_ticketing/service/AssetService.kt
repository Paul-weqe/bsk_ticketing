package com.example.bsk_ticketing.service

import com.example.bsk_ticketing.dto.mapper.AssetMapper
import com.example.bsk_ticketing.dto.model.AssetDto
import com.example.bsk_ticketing.exception.AssetNotFoundException
import com.example.bsk_ticketing.repository.AssetRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AssetService : IAssetService{

    @Autowired
    lateinit var assetRepository: AssetRepository

    fun getAllAssets(): ArrayList<AssetDto>{
        val assets = assetRepository.findAll()
        val assetsDto = ArrayList<AssetDto>()
        assets.forEach{
            assetsDto.add(AssetMapper.toAssetDto(it))
        }
        return assetsDto
    }

    fun createAsset(assetDto: AssetDto): AssetDto{
        val asset = assetRepository.save(AssetMapper.toAssetModel(assetDto))
        return AssetMapper.toAssetDto(asset)
    }

    fun getAssetById(assetId: Int): AssetDto{
        val asset = assetRepository.findById(assetId)
//        if (asset.isEmpty) throw AssetNotFoundException("Asset with ID $assetId not found")
        return AssetMapper.toAssetDto(asset.get())
    }

}