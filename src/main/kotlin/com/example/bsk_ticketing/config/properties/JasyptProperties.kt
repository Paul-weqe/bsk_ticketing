package com.example.bsk_ticketing.config.properties

import lombok.Getter
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

@Getter
@Configuration
@PropertySource(value = ["file:\${CONFIG_PATH}/organization.properties"])
class JasyptProperties {
    @Value("\${org.app.properties.jasypt.encryptor.password}")
    val password: String? = null
    @Value("\${org.app.properties.jasypt.encryptor.algorithm}")
    val algorithm: String? = null
    @Value("\${org.app.properties.jasypt.encryptor.keyObtentionIterations}")
    val keyObtentionIterations: String? = null
    @Value("\${org.app.properties.jasypt.encryptor.poolSize}")
    val poolSize: String? = null
    @Value("\${org.app.properties.jasypt.encryptor.providerName}")
    val providerName: String? = null
    @Value("\${org.app.properties.jasypt.encryptor.saltGeneratorClassname}")
    val saltGeneratorClassname: String? = null
    @Value("\${org.app.properties.jasypt.encryptor.stringOutputType}")
    val stringOutputType: String? = null
}