package com.example.bsk_ticketing.config

import com.example.bsk_ticketing.config.Handlers.SignOutSuccessHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.util.matcher.AntPathRequestMatcher

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
class SecurityConfig : WebSecurityConfigurerAdapter() {

    @Autowired
    lateinit var userDetailsService: UserDetailsServiceImpl

    override fun configure(http: HttpSecurity?) {
        http!!
        .csrf().disable()
                .authorizeRequests()
                .antMatchers("/**", "/dist/**", "/plugins/**", "/css/**", "/images/**").permitAll()
                .antMatchers("/tickets", "/tickets/", "/tickets/**", "/tickets/all-open-tickets", "/tickets/all-open-tickets/",
                        "/tickets/all-open-tickets/**", "/tickets/all-closed-tickets", "/tickets/all-closed-tickets/", "/tickets/all-closed-tickets/**",
                        "/create-ticket", "/create-ticket/", "/create-ticket/**", "/view-assets/**", "/view-assets/", "/view-assets")
                .hasAuthority("ROLE_PASS_CONFIRMED")
                .antMatchers("/user-login").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .formLogin()
                .loginPage("/user-login")
                .loginProcessingUrl("/support-portal/app-login")
                .usernameParameter("app_email")
                .passwordParameter("app_password")
                .permitAll()
                .defaultSuccessUrl("/tickets/all-open-tickets", true)
                .failureUrl("/user-login?error=true")
//                .failureHandler(authenticationFailureHandler())
                .and().logout()
                .logoutUrl("/user-logout")
                .logoutSuccessUrl("/support-portal/user-login")
                .logoutRequestMatcher(AntPathRequestMatcher("/portal/service/app-logout"))
                .logoutSuccessHandler(logOutSuccessHandler())
                .and().exceptionHandling() //exception handling configuration
                .accessDeniedPage("/access-denied")
    }

    @Autowired
    fun configureGlobal(auth: AuthenticationManagerBuilder?) {
        val passwordEncoder = BCryptPasswordEncoder()
        auth!!.userDetailsService<UserDetailsService>(userDetailsService).passwordEncoder(passwordEncoder)
    }

    @Bean
    fun encoder(): BCryptPasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    fun logOutSuccessHandler(): SignOutSuccessHandler {
        return SignOutSuccessHandler()
    }

//    @Bean
//    fun authenticationFailureHandler(): AuthFailureHandler {
//        return authenticationFailureHandler()
//    }
}