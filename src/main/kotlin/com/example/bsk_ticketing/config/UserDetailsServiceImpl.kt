package com.example.bsk_ticketing.config

import com.example.bsk_ticketing.repository.UserRepository
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import java.util.logging.Level
import java.util.logging.Logger

@Service
class UserDetailsServiceImpl(val userRepository: UserRepository): UserDetailsService {

    override fun loadUserByUsername(p0: String?): UserDetails {
        println("Username: {$p0}" )
        val user = userRepository.findByEmail(p0!!)
        Logger.getLogger(javaClass.name).log(Level.INFO, "Email: [${user!!.email}]")
        Logger.getLogger(javaClass.name).log(Level.INFO, "Granted Authorities: [${user.getGrantedAuthorities()}]")

        return AppUserPrincipal(user)
    }
}