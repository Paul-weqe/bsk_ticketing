package com.example.bsk_ticketing.config

class PortalConstants {

    companion object {
        const val PASS_APPROVED: String = "ROLE_PASS_CONFIRMED"
        const val ROLE_USER: String = "ROLE_USER"
        const val ROLE_ADMIN: String = "ROLE_ADMIN"
        const val ROLE_SUPER_ADMIN: String = "ROLE_SUPER_ADMIN"
        const val PAGE_SIZE = 10
    }
}