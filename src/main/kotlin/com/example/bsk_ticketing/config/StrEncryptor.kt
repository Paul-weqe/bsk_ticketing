package com.example.bsk_ticketing.config

import com.example.bsk_ticketing.config.properties.JasyptProperties
import org.jasypt.encryption.StringEncryptor
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

@Component
class StrEncryptor {
    @Autowired
    lateinit var jasyptProperties: JasyptProperties

    @Bean("jasyptStringEncryptor")
    fun stringEncryptor(): StringEncryptor {
        val encryptor = PooledPBEStringEncryptor()
        val config = SimpleStringPBEConfig()
        config.password = jasyptProperties.password
        config.algorithm = jasyptProperties.algorithm
        config.setKeyObtentionIterations(jasyptProperties.keyObtentionIterations)
        config.setPoolSize(jasyptProperties.poolSize)
        config.providerName = jasyptProperties.providerName
        config.setSaltGeneratorClassName(jasyptProperties.saltGeneratorClassname)
        config.stringOutputType = jasyptProperties.stringOutputType
        encryptor.setConfig(config)
        return encryptor
    }

}