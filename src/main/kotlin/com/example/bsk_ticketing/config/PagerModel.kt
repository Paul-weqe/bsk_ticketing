package com.example.bsk_ticketing.config

import lombok.Data

@Data
class PagerModel(totalPages: Int, currentPage: Int, buttonsToShow: Int) {
    var buttonsToShow = 5
        set(buttonsToShow) {
            field = this.buttonsToShow
        }

    var startPage: Int = 0
        set(startPage) {
            field = this.startPage
        }

    var endPage: Int = 0
        set(endPage) {
            field = this.endPage
        }

    init {

        this.buttonsToShow = buttonsToShow

        val halfPagesToShow = buttonsToShow / 2

        if (totalPages <= buttonsToShow) {
            startPage = 1
            endPage = totalPages
        } else if (currentPage - halfPagesToShow <= 0) {
            startPage = 1
            endPage = buttonsToShow
        } else if (currentPage + halfPagesToShow == totalPages) {
            startPage = currentPage - halfPagesToShow
            endPage = totalPages
        } else if (currentPage + halfPagesToShow > totalPages) {
            startPage = totalPages - buttonsToShow + 1
            endPage = totalPages
        } else {
            startPage = currentPage - halfPagesToShow
            endPage = currentPage + halfPagesToShow
        }

    }
}