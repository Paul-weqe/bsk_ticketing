package com.example.bsk_ticketing.config.Handlers

import org.springframework.security.core.Authentication
import javax.servlet.ServletException
import java.io.IOException
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpServletRequest
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler


class SignOutSuccessHandler : SimpleUrlLogoutSuccessHandler(), LogoutSuccessHandler {

    // API

    @Throws(IOException::class, ServletException::class)
    override fun onLogoutSuccess(request: HttpServletRequest, response: HttpServletResponse, authentication: Authentication) {
        val refererUrl = request.getHeader("Referer")
        println(refererUrl)

        super.onLogoutSuccess(request, response, authentication)
    }

}