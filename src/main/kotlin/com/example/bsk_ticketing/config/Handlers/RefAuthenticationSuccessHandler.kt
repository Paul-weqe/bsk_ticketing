package com.example.bsk_ticketing.config.Handlers

import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler


class RefAuthenticationSuccessHandler : SimpleUrlAuthenticationSuccessHandler(), AuthenticationSuccessHandler {
    init {
        setUseReferer(true)
    }

}